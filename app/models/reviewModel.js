const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const reviewSchema = new Schema({
  _id: {
    type: mongoose.Types.ObjectId 
  },
  stars: {
    type: Number,
    defaut: 0
  },
  note: {
    type: String,
    required: false
  },
  createdAt: {
    type: Date,
    default: Date.now()
  },
  updatedAt: {
    type: Date,
    default: Date.now()
  }
});

module.exports = mongoose.model("review", reviewSchema);