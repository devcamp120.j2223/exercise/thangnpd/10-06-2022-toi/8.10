const express = require('express');
const router = express.Router();

//Import các hàm trong reviewController
const {
  createReviewOfCourse,
  getAllReviewOfCourse,
  getReviewById,
  updateReviewById,
  deleteReviewById
} = require('../controllers/reviewController');

router.post("/courses/:courseId/reviews", createReviewOfCourse);

router.get("/courses/:courseId/reviews", getAllReviewOfCourse);

router.get("/reviews/:reviewId", getReviewById);

router.put("/reviews/:reviewId", updateReviewById);

router.delete("/courses/:courseId/reviews/:reviewId", deleteReviewById);

module.exports = router;